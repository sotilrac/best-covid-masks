# Reusable face masks in three sizes to best COVID

![](wearing-masks.jpg)

## Objective 
We want to create reusable and reliable face masks with a stylish touch to help mitigate the spread of COVID-19. 

We have a social responsibility to protect ourselves and the people around us. At the moment, the scientific consensus is that wearing face masks is an effective way of achieving this. 

As it is now familiar to everyone, it was not that easy to source face masks and it turns out sewing face masks at home is not that easy either. So we researched and compiled the best design methods to adapt a design that is simple and customizable. Our design features a three-layered construction with a pocket filter. 

![patterns/all-patterns-small.png](patterns/all-patterns.png)

## Materials 
- ![Pattern Print or Tracing](patterns)
- Cotton Fabric 
- Sewing Pins
- Scissors 
- AWG Copper Wire 
- Iron + Ironing Board 
- Sewing Thread 
- Elastic Yarn or Elastic Cord and Locks  
- Sewing Machine 

## Features
- Three-Layered Construction
- Adjustable Nose Wire & Ear Straps 
- Filter Pocket 
- Machine Washable 
- Reusable 


## How to

![](masks-production.jpg)

We made our own adaptations of the Olson hospital face mask patterns to better fit smaller faces.

Follow the instructions below for assembly and see the modifications to the method below.

[![](http://img.youtube.com/vi/StMPYZ-waso/0.jpg)](http://www.youtube.com/watch?v=StMPYZ-waso "")

### Modifications: 
We preferred to stitch the top and bottom edges of the mask instead of around for sake of expediency.


### For use: 
Place mask over top of nose, adjusting nose wire accordingly. Bottom portion of the mask should lie below the chin. Secure mask around ears with elastic straps. Ensure mask fits comfortably. If too tight or loose, untie ear straps’ knots and re-tie with a double knot. 

For masks with locks, stretch cords in a parallel position to introduce mask over head. Mask cloth covers face from top of nose to bottom of chin. Upper part of elastic cord rests above ears and around back of head. Lower part of elastic cord lies below ears and can be adjusted with cord lock for enjoyable wear. 


## Care

- Replace coffee filter when washing the mask.
- After removing the copper wire, wash mask in warm water and air or tumble dry.
- Avoid ironing over the elastic. 


This design is not intended to replace medical-grade personal protective equipment. Use of this mask while following social distancing measures, washing your hands and refraining from touching your face. 
