# Mascarillas reusables en tres tamaños.

![](wearing-masks.jpg)

## Objetivo
Queremos crear mascarillas reusables y fiables con un toque elegante para ayudar a mitigar el brote del COVID-19.

Tenemos la responsabilidad social de protegernos a nosotros mismos y a las personas que nos rodean. Por el momento, el consenso científico señala que usar mascarillas faciales es una manera efectiva de lograrlo.

Como ahora muchos pueden corroborar, no es tan fácil obtener mascarillas y resulta que coserlas en casa tampoco es tan fácil. Así que investigamos y reunimos los mejores métodos de diseño para adaptar un diseño que sea simple y personalizable. Nuestro diseño cuenta con una construcción de tres capas así como un bolsillo para agregar un filtro.

![patterns/all-patterns-small.png](patterns/all-patterns.png)

## Materiales
- ![Trazos](patterns)
- Tela de algodón
- Alfileres 
- Tijeras
- Alambre de cobre AWG
- Plancha + tabla de planchar
- Hilo de coser
- Hilo elástico o cordón elástico y cerraduras
- Máquina de coser

## Características
- Construcción de tres capas
- Alambre de nariz ajustable y correas para las orejas 
- Bolsillo para un filtro
- Lavable a máquina 
- Reusable


## Cómo

![](masks-production.jpg)

Adaptamos los trazos de las mascarillas del hospital Olson para satisfacer tamaños más pequeños.  

Siga las instrucciones de ensamblaje a continuación y vea las modificaciones al método. 

[![](http://img.youtube.com/vi/StMPYZ-waso/0.jpg)](http://www.youtube.com/watch?v=StMPYZ-waso "")

### Modificaciones:
Preferimos coser el borde superior e inferior de la mascarilla en lugar de todo el alrededor por motivos de conveniencia.


### Uso:
Coloque la mascarilla sobre la parte superior de la nariz, ajustando el alambre de la nariz como corresponda. La parte inferior de la mascarilla debe estar debajo del mentón. Asegure la mascarilla alrededor de las orejas con correas elásticas. Asegure que la mascarilla se ajuste cómodamente. Si está demasiado apretada o suelta, desate los nudos de las correas para las orejas y vuelva a atarlas con un nudo doble.

Para las mascarillas con cerraduras, estire los cordones elásticos en una posición paralela para poder introducir la máscara sobre la cabeza. El paño de la máscara cubre la cara desde la parte superior de la nariz hasta la parte inferior del mentón. La parte superior del cordón elástico queda sobre las orejas y alrededor de la parte posterior de la cabeza. La parte inferior del cordón elástico se encuentra debajo de las orejas y se puede ajustar para su comodidad. 


## Cuidado
 
- Reemplace el filtro de café cuando lave la mascarilla.
- Después de retirar el alambre de cobre, lave la mascarilla con agua tibia y séquela al aire o a máquina. 
- Evite planchar sobre el elástico.


Este diseño no pretende reemplazar el equipo de protección personal de grado médico. Use esta máscara mientras sigue las medidas de distanciamiento social, lavándose las manos y evitando tocarse la cara.

